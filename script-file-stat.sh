#!/bin/bash
if [ ! -z "$1" -a -f "$1" ]
then
    TYPE=$(file -b $1)
    DROITS=$(stat --format='%A (%a)' $1)
    echo "Type du fichier '$1' :"
    echo "        ${TYPE}"
    echo "Droits d'accès :"
    echo "        ${DROITS}"
    #ou bien d'une facon plus condensée
    #echo -e "Type du fichier $1:\n\t${TYPE}\nDroits d'acces:\n\t${DROITS}"
else
    echo "$0: Aucun fichier ordinaire correct n'a été fourni en paramètre" >&2
fi
