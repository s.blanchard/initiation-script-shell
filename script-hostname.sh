#!/bin/bash
FICHIER=/etc/hostname
if [ -f $FICHIER ]
then
    cat $FICHIER
else
    echo "Le fichier $FICHIER n'existe pas ou n'est pas un fichier ordinaire" >&2
fi
