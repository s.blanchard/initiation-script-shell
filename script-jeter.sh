#!/bin/bash
POUBELLE=$HOME/poubelle
if [ ! -e $POUBELLE ]
then
    mkdir $POUBELLE
elif [ ! -d $POUBELLE ]
then
    echo "Le fichier $POUBELLE existe déjà et n'est pas un répertoire" >&2
    exit 1
fi

case $1 in
    -l) echo "contenu de la poubelle :"
        ls $POUBELLE
        ;;
    -c) echo "la poubelle se vide :"
        rm -Rfv $POUBELLE/*
        ;;
    *)
        for PARAM in $@
        do
            if [ -e $PARAM ]
            then
                mv $PARAM $POUBELLE/
                if [ $? ]
                then
                    echo "'$PARAM' est maintenant à la poubelle"
                fi
            else
                echo "'$PARAM' n'existe pas" >&2
            fi
        done
        ;;
esac
