#!/bin/bash
if [ "$1" ]
then
    pids=$(pidof $1)
    if [ "$pids" = "" ]
    then
        echo "Aucun PID pour le programme $1" >&2
        exit 2
    else
        echo "PID du programme $1"
        for pid in $pids
        do
            echo $pid
        done
    fi
else
    echo "Aucun nom de programme n'a ete fourni" >&2
    exit 1
fi
